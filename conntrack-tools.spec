#needsrootforbuild
Name:             conntrack-tools
Version:          1.4.8
Release:          2
Summary:          Userspace tools for interacting with the Connection Tracking System
License:          GPLv2
URL:              http://conntrack-tools.netfilter.org/
Source0:          http://netfilter.org/projects/conntrack-tools/files/%{name}-%{version}.tar.xz
Source1:          conntrackd.service
Source2:          conntrackd.conf

BuildRequires:    libnfnetlink-devel >= 1.0.1 libnetfilter_conntrack-devel >= 1.0.9 libtirpc-devel
BuildRequires:    libnetfilter_cttimeout-devel >= 1.0.0 libnetfilter_cthelper-devel >= 1.0.0 systemd
BuildRequires:    libmnl-devel >= 1.0.3 libnetfilter_queue-devel >= 1.0.2 pkgconfig bison flex systemd-devel
BuildRequires:    gcc make
Provides:         conntrack = 1.0-1
Obsoletes:        conntrack < 1.0-1
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

%description
The conntrack-tools are a set of userspace tools for Linux that allow system administrators
interact with the Connection Tracking System, which is the module that provides stateful packet
inspection for iptables.The tools can be used to search, list,inspect and maintain the connection
tracking subsystem of the Linux kernel. The conntrack-tools are the userspace daemon conntrackd
and the command line interface conntrack.

%package          help
Summary:          Help for conntrack-tools
BuildArch:        noarch

%description      help
This package contains man manual for conntrack-tools.

%prep
%autosetup -n conntrack-tools-%{version} -p1

%build
%configure --disable-static --enable-systemd
sed -i "s/DEFAULT_INCLUDES = -I./DEFAULT_INCLUDES = -I. -I\/usr\/include\/tirpc/" src/helpers/Makefile
CFLAGS="${CFLAGS} -Wl,-z,lazy"
CXXFLAGS="${CXXFLAGS} -Wl,-z,lazy"

%make_build
rm -f doc/sync/notrack/conntrackd.conf.orig doc/sync/alarm/conntrackd.conf.orig doc/helper/conntrackd.conf.orig
chmod 644 doc/sync/primary-backup.sh
rm -rf doc/misc

%install
%make_install
%delete_la
install -d      %{buildroot}%{_sysconfdir}/conntrackd
install -d 0755 %{buildroot}%{_unitdir}
install -m644   %{SOURCE1} %{buildroot}%{_unitdir}/
install -m644   %{SOURCE2} %{buildroot}%{_sysconfdir}/conntrackd/

%check
cd tests/conntrack
sh run-test.sh
cd ../nfct
sed -i "s/\/usr\/sbin/..\/..\/src/g" test.c
sh run-test.sh
cd ../../

%post
%systemd_post conntrackd.service

%preun
%systemd_preun conntrackd.service

%postun
%systemd_postun conntrackd.service

%files
%doc AUTHORS TODO doc COPYING
%dir %{_sysconfdir}/conntrackd
%config(noreplace) %{_sysconfdir}/conntrackd/conntrackd.conf
%{_unitdir}/conntrackd.service
%{_sbindir}/{conntrack,conntrackd,nfct}
%dir %{_libdir}/conntrack-tools
%{_libdir}/conntrack-tools/*

%files help
%{_mandir}/man5/*
%{_mandir}/man8/*

%changelog
* Tue Aug 27 2024 yanglu <yanglu72@h-partners.com> - 1.4.8-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:delete the misc directory and clusterip.sh and README files

* Thu Dec 28 2023 yanglu <yanglu72@h-partners.com> - 1.4.8-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update conntrack-tools version to 1.4.8

* Tue Jan 31 2023 yanglu <yanglu72@h-partners.com> - 1.4.7-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update conntrack-tools version to 1.4.7

* Thu Jan 19 2023 yanglu <yanglu72@h-partners.com> - 1.4.6-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Optimize enabled DT testcases

* Mon Jan 09 2023 zhanghao <zhanghao383@huawei.com> - 1.4.6-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:enabled DT testcases

* Mon Oct 31 2022 yanglu <yanglu72@h-partners.com> - 1.4.6-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:conntrack:Fix for memleak when parsing -j arg

* Thu Apr 28 2022 yanglu <yanglu72@h-partners.com> - 1.4.6-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:rebuild package

* Thu May 27 2021 lijingyuan <lijingyuan3@huawei.com> - 1.4.6-2
- Type:bugfix 
- ID:NA
- SUG:NA
- DESC:Add the compilation dependency of gcc.

* Sat Jul 25 2020 gaihuiying <gaihuiying1@huawei.com> - 1.4.6-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update conntrack-tools version to 1.4.6

* Tue Dec 31 2019 Tianfei <tianfei16@huawei.com> - 1.4.4-9
- Package init
